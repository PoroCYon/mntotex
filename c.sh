#!/usr/bin/env bash

EXTRAOPTS=

if ! [ -z "$RELEASE" ]; then
    EXTRAOPTS="--standalone --staticlink:Ionic.Zip.Reduced"
else
    EXTRAOPTS="--debug:pdbonly --nocopyfsharpcore"
fi

if ! [ -d bin ]; then mkdir -p bin; fi

fsharpc --lib:ref --tailcalls+ --target:exe \
    --nooptimizationdata --nointerfacedata --optimize+ \
    --crossoptimize+ --checked- --out:bin/mntotex.exe \
    -r:Ionic.Zip.Reduced.dll \
    $EXTRAOPTS \
    src/types.fs src/util.fs src/parse.fs src/write.fs src/file.fs \
    src/main.fs

