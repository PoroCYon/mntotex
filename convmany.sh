#!/usr/bin/env bash

BINPATH="$(dirname "$0")"
TMPDIR="/tmp/mntotex-convmany"

if ! command -v mono >/dev/null 2>&1; then
    >&2 cat <<EOF
The mono runtime is not installed! Cannot continue...

Press enter to close this window.
EOF
    read
    exit 1
fi

if [ -d "$TMPDIR" ]; then
    >&2 cat <<EOF
Sorry, can't run more than once at the same time...

Press enter to close this window.
EOF
    read
    exit 1
fi

while ! [ -z "$@"]; do
    FILE="$1"
    FILN="$(basename "$FILE" ".mn")"
    mkdir -p "$TMPDIR/$FILN"
    mono "$BINPATH" -o "$TMPDIR/$FILN" "$FILE" && \
        mv "$TMPDIR/$FILN/_sheet0.tex" "$FILE.tex" || \
        (>&2 echo "Couldn't convert $FILN..."; sleep 1)
done
rm -rf "$TMPDIR"

cat <<EOF
Done! You can edit the LaTeX files to your liking (e.g. add a title),"
and compile them.

Press enter to close this window.
EOF
read

