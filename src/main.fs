
open System
open System.IO

open MNToTex
open MNToTex.Parse
open MNToTex.Write
open MNToTex.File

[<EntryPoint>]
let main args =
    if Array.isEmpty args then do
        eprintfn "Usage: mntotex [-o <output dir>] <files to convert...>"
        Environment.Exit (0)

    let (outdir, argn) = if args.[0] = "-o" then args.[1], 2 else ".", 0
    let arg_ = Array.skip argn args

    for f in arg_ do
        if File.Exists f
        then try convertMN outdir (Array.length arg_ > 1) f
             with _ as e -> eprintfn "Error:\n%O" e
        else eprintfn "Error: file '%s' doesn't exist." f

    0

