module MNToTex.File

open System
open System.Collections
open System.Collections.Generic
open System.IO
open System.Linq
open System.Text
open System.Xml
open Ionic.Zip

open MNToTex
open MNToTex.Parse
open MNToTex.Write

let private readTextE (zf: ZipFile) name =
    if zf.ContainsEntry name then
        let ze = zf.[name]
        use ms = new MemoryStream ()
        ze.Extract ms
        Some <| Encoding.UTF8.GetString (ms.ToArray ())
    else None

let convertMN (outd: string) (mksubdir: bool) (f: string) =
    use zf = try ZipFile.Read (f)
             with _ as e -> failwith "Not a zip file."

    match readTextE zf "_mimetype.txt" with
    | Some "application/x-mupad-notebook+zip\n" -> ()
    | _ -> failwith "Invalid MIME type file, or none found."

    let content = XmlDocument ()
    match readTextE zf "_content.xml" with
    | Some s -> content.LoadXml s
    | _      -> failwith "'_content.xml' file not found."

    let cntx = content.ChildNodes.[1]

    for x in cntx.ChildNodes |> enToSeq<XmlNode> do
        let n = getAttr "name" x |> Option.get
        match x.Name with
        | "plot"  -> eprintfn "Note: can't export plot '%s': not implemented." n
        | "sheet" ->
            eprintfn "Exporting sheet '%s'" n

            use ms = new MemoryStream ()
            zf.[n].Extract ms
            ms.Seek (0L, SeekOrigin.Begin) |> ignore

            let nb = readNotebook ms
            let fn = outd + (if mksubdir
                             then "/" + Path.GetFileNameWithoutExtension f + "/"
                             else "/")
                        + n.Replace (".xml", ".tex")
            let fnd = Path.GetDirectoryName fn
            if not <| Directory.Exists fnd then Directory.CreateDirectory fnd |> ignore
            File.WriteAllText (fn, mkLaTeX nb)
        | _ -> eprintfn "Note: unknown file type '%s'" x.Name

    eprintfn "Done."

