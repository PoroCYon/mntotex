module MNToTex.Write

open System
open System.Collections.Generic
open System.Linq
open System.Text

open MNToTex

let private fontLookup (mnfnt: string) =
    match mnfnt with
    | "Generic Sans Serif" -> "cmss"
    | "Generic Monospace"  -> "cmtt"
    | "Generic Serif"      -> "cmr"
    | _                    -> mnfnt

let private withStyle (F: 'a -> string) (st: Style option) (x: 'a) =
    match st with
    | Some (TextSt (f, s, c)) | Some (MathSt (f, s, c)) ->
        let fn = match c with "" -> id
                            | _  -> sprintf "{\\color[HTML]{%s}%s}\n" (c.TrimStart ('#'))
        in fn <| sprintf "{\\fontfamily{%s%s}\\selectfont\n%s}\n"
                (fontLookup f)
                (match s with Some 11 | None -> String.Empty | Some sz -> string sz)
                (F x)
    | Some (ParaSt (a, w)) -> F x
    | Some (CalcSt (tm, bm, p)) -> F x
    | Some (TablSt p) | Some (FramSt p) -> F x
    | _ -> F x

let private lSt (styles: IDictionary<string, Style>) (st: string) =
    let (has, v) = styles.TryGetValue (st)
    if has then Some v else None

let rec private renderVis (styles: IDictionary<string, Style>) (v: Vis) =
    match v with
    | Span (st,  ctmv) ->
        withStyle id (lSt styles st) <|
            match ctmv with
            | Choice1Of2 c  -> fix c
            | Choice2Of2 mv -> sprintf "$%s$" mv.tex
    | Para (st, ts, c) ->
        withStyle (withStyle id (lSt styles ts)) (lSt styles st) <|
            String.Join (" ", Seq.map (renderVis styles) c)
    | Math (st, mv   ) -> withStyle id (lSt styles st) <| sprintf "$$%s$$" mv.tex
// sorry, but I'm lazy
let rec private renderVisLst (styles: IDictionary<string, Style>) (v: Vis) =
    match v with
    | Span (st,  ctmv) -> match ctmv with
                          | Choice1Of2 c  -> fix c
                          | Choice2Of2 mv -> sprintf "$%s$" mv.tex
    | Para (st, ts, c) ->
        withStyle (withStyle id (lSt styles ts)) (lSt styles st) <|
            (sprintf "\\begin{lstlisting}\n%s\n\\end{lstlisting}\n" <|
                String.Join (String.Empty, Seq.map (renderVisLst styles) c))
    | Math (st, mv   ) -> withStyle id (lSt styles st) <| sprintf "$$%s$$" mv.tex


let private render (styles: IDictionary<string, Style>) (body: Body) =
    match body with
    | BVis  v -> renderVis styles v
    | BCalc c ->
        withStyle (fun _ ->
            let sb = StringBuilder ()
            match c.muIn with
            | Some v ->
                //sb.AppendLine ("\\begin{lstlisting}") |> ignore
                for x in v.content do
                    sb.AppendLine (renderVisLst styles x) |> ignore
                //sb.AppendLine ("\\end{lstlisting}"  ) |> ignore
            | _      -> ()
            match c.cStep with
            | Some v ->
                for x in v.out.content do
                    sb.AppendLine (renderVis styles x) |> ignore
            | _      -> ()

            sb.ToString ()
        ) (lSt styles c.st) ()

let mkLaTeX (nb: Notebook) =
    // TODO: make this cleaner
    sprintf "\\documentclass[9pt,a4paper]{extarticle}\n\\usepackage[utf8]{inputenc}\n\\usepackage{listings,amssymb,graphicx,amsfonts}\n\\usepackage[intlimits]{amsmath}\n\\usepackage[usenames,dvipsnames,svgnames,table]{xcolor}\n\\title{%s}\n\\author{TODO}\n\\date{}\n\\begin{document}\n\\maketitle\n%s\n\\end{document}\n"
        nb.title (String.Join ("\n\n", Seq.map (render nb.styles) nb.body))

