
#r "Ionic.Zip.Reduced.dll"

#load "types.fs";;
#load "util.fs";;
#load "parse.fs";;
#load "write.fs";;
#load "file.fs";;

open MNToTex.File

for f in fsi.CommandLineArgs |> Seq.skipWhile ((<>) "--") |> Seq.tail do
    convertMN "out" true f

