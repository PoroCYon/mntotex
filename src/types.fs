namespace MNToTex

open System
open System.Collections.Generic
open System.Linq

type Align = Left    = 0
           | Right   = 1
           | Top     = 2
           | Bottom  = 3
           | Center  = 4
           | Justify = 5

type Style = TextSt of family: string * size: int option * color: string
           | MathSt of family: string * size: int option * color: string
           | ParaSt of alignment: Align option * wrap: int option
           | CalcSt of topMargin: int option * bottomMargin: int option * padding: int option
           | PageSt
           | TablSt of padding: int option
           | FramSt of padding: int option
           | ListSt

type MathV = { expr: string; tex: string } // also contains other unneeded rubbish

type Vis = Span of st: string * content: Choice<string, MathV>
         | Para of st: string * textSt: string * content: Vis seq
         | Math of st: string * content: MathV

type InOut = { id: string; content: Vis seq }

//type Output = { id: string; content: Vis seq }
type CStep = { op: string; out: InOut }

type Calc = { st: string; muIn: InOut option; cStep: CStep option }

type Body = BVis of Vis | BCalc of Calc

type Notebook = { title: string;
                  styles: IDictionary<string, Style>;
                  body: Body seq }

