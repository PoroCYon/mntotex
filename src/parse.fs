module MNToTex.Parse

open System
open System.Collections
open System.Collections.Generic
open System.IO
open System.Linq
open System.Xml

open MNToTex

let private parseToInt (st: string) =
    //printfn "i: %s" st
    let (s, o) = Int32.TryParse (st)
    if s then Some o
    else
        let (s', o') = Single.TryParse (st)
        if s' then Some <| int o' else None

let private readStyles (styles: XmlNode) =
    styles.ChildNodes |> enToSeq<XmlNode> |> Seq.collect (fun c ->
        match c.Name with
        | "textStyle" ->
            let id  = getAttr "id"     c |> Option.get
            let fam = getAttr "family" c |> flip defaultArg String.Empty
            let sz  = getAttr "size"   c |> Option.bind parseToInt
            let col = getAttr "color"  c |> flip defaultArg String.Empty
            in [(id, TextSt (fam, sz, col))]
        | "mathStyle" ->
            let id  = getAttr "id"     c |> Option.get
            let fam = getAttr "family" c |> flip defaultArg String.Empty
            let sz  = getAttr "size"   c |> Option.bind parseToInt
            let col = getAttr "color"  c |> flip defaultArg String.Empty
            in [(id, MathSt (fam, sz, col))]
        | "paraStyle" ->
            let id = getAttr "id"        c |> Option.get
            let al = getAttr "alignment" c |> Option.bind tryReadEnum<Align>
            let wr = getAttr "wrap"      c |> Option.bind parseToInt
            in [(id, ParaSt (al, wr))]
        | "calcStyle" ->
            let id = getAttr "id"           c |> Option.get
            let tM = getAttr    "topMargin" c |> Option.bind parseToInt
            let bM = getAttr "bottomMargin" c |> Option.bind parseToInt
            let pd = getAttr "padding"      c |> Option.bind parseToInt
            in [(id, CalcSt (tM, bM, pd))]
        | "pageStyle" ->
            let id = getAttr "id" c |> Option.get
            in [(id, PageSt)]
        | _ -> List.empty
    ) |> dict;;

let rec private readVis (c: XmlNode) =
    match c.Name with
    | "p"    ->
        let st = getAttr "style"     c |> Option.get
        let ts = getAttr "textStyle" c |> Option.get
        let cs = c.ChildNodes |> enToSeq<XmlNode> |> Seq.collect readVis
        in [Para (st, ts, cs)]
    | "s"    ->
        let st = getAttr "style" c |> Option.get
        let cn = c.ChildNodes |> enToSeq<XmlNode>
        let cs = match cn |> Seq.tryFind (fun n -> n.Name = "math") with
                 | Some m ->
                    match readVis m with
                    | [Math (_, mv)] -> Choice2Of2 mv
                    | _              -> failwith "Bad 'math' node? This shouldn't happen."
                 | _      -> Choice1Of2 c.InnerText
        in [Span (st, cs)]
    | "math" ->
        let st = getAttr "style" c |> Option.get
        let ex = getAttr "expr"  c |> Option.get
        let tx = getAttr "tex"   c |> Option.get
        in [Math (st, {expr=ex; tex=tx})]
    |  _     -> List.empty

let private readIn    (elems: XmlNode) =
    let lang = elems.FirstChild
    let id   = getAttr "id" elems |> Option.get
    let chds = lang.ChildNodes |> enToSeq<XmlNode>
    in { id = id; content = chds |> Seq.collect readVis }
let private readCStep (elems: XmlNode) =
    let chds = elems.ChildNodes |> enToSeq<XmlNode>
    let op   = chds |> Seq.tryFind (fun c -> c.Name = "muOp" )
                    |> Option.bind (getAttr "name") |> Option.get
    let out  = chds |> Seq.tryFind (fun c -> c.Name = "muOut") |> Option.get
    let oid  = out |> getAttr "id" |> Option.get
    let cs   = out.ChildNodes |> enToSeq<XmlNode> |> Seq.collect readVis
    let oup  = { id = oid; content = cs }
    in { op = op; out = oup }
let private readCalc  (elems: XmlNode) =
    let cs = elems.ChildNodes |> enToSeq<XmlNode>
    let muIn = cs |> Seq.tryFind (fun c -> c.Name = "muIn" )
    let step = cs |> Seq.tryFind (fun c -> c.Name = "cStep")
    let st = getAttr "style" elems |> Option.get
    in [{ st = st; muIn = Option.map readIn muIn; cStep = Option.map readCStep step; }]

let private readBody (elems: XmlNode) =
    elems.ChildNodes |> enToSeq<XmlNode> |> Seq.collect (fun c ->
        match c.Name with
        | "p"    -> readVis  c |> Seq.map BVis
        | "calc" -> readCalc c |> Seq.map BCalc
        | _      -> Seq.empty
    );;

let readNotebook (file: Stream) =
    let d = XmlDocument ()
    d.Load (file)
    let sh = d.ChildNodes.[1].ChildNodes |> enToSeq<XmlNode>
    let t = sh |> Seq.tryFind (fun c -> c.Name = "title" ) |> Option.get
    let s = sh |> Seq.tryFind (fun c -> c.Name = "styles") |> Option.get
    let b = sh |> Seq.tryFind (fun c -> c.Name = "body"  ) |> Option.get
    in { title = t.InnerText; styles = readStyles s; body = readBody b }

