[<AutoOpen>]
module MNToTex.Utils

open System
open System.Collections
open System.Collections.Generic
open System.IO
open System.Linq
open System.Xml

let readXml (s: string) =
    let d = XmlDocument ()
    d.LoadXml (s)
    d

let inline flip f y x = f x y
let inline enToSeq<'a> (e: IEnumerable): 'a seq =
    seq [ for x in e do yield x :?> 'a ]
let inline tryReadEnum<'a when 'a : (new : unit -> 'a) and 'a : struct and 'a :> ValueType> (s: string): 'a option =
    let (succ, v) = Enum.TryParse<'a> s in if succ then Some v else None

let getAttr n (xn: XmlNode) =
    //printfn "n: %s" n
    xn.Attributes |> enToSeq<XmlAttribute>
                  |> Seq.tryFind (fun a -> a.Name = n)
                  |> Option.map (fun a -> a.InnerText);;

let private fixes = [
    ('#', "\\#"); ('&', "\\&"); (char 8232, Environment.NewLine);
    ('Α', "$A$"); ('α', "$\\alpha$"); ('Β', "$B$"); ('β', "$\\beta$"); ('Γ', "$\\Gamma$");
    ('γ', "$\\gamma$"); ('Δ', "$\\Delta$"); ('δ', "$\\delta$"); ('Ε', "$E$");
    ('ε', "$\\varepsilon$"); ('Ζ', "$Z$"); ('ζ', "$\\zeta$"); ('Η', "$H$");
    ('η', "$\\eta$"); ('Θ', "$\\Theta$"); ('θ', "$\\theta$"); ('Ι', "$I$");
    ('ι', "$\\iota$"); ('Κ', "$K$"); ('κ', "$\\kappa$"); ('Λ', "$\\Lambda$");
    ('λ', "$\\lambda$"); ('Μ', "$M$"); ('μ', "$\\mu$"); ('Ν', "$N$"); ('ν', "$\\nu$");
    ('Ξ', "$\\Ksi$"); ('ξ', "$\\ksi$"); ('Ο', "$O$"); ('ο', "$o$"); ('Π', "$\\Pi$");
    ('π', "$\\pi$"); ('Ρ', "$P$"); ('ρ', "$\\rho$"); ('Σ', "$\\Sigma$");
    ('σ', "$\\sigma$"); ('ς', "$\\varsigma$"); ('Τ', "$T$"); ('τ', "$\\tau$");
    ('Υ', "$\\Upsilon$"); ('υ', "$\\upsilon$"); ('Φ', "$\\Phi$"); ('φ', "$\\varphi$");
    ('Χ', "$X$"); ('χ', "$\\chi$"); ('Ψ', "$\\Psi$"); ('ψ', "$\\psi$");
    ('Ω', "$\\Omega$"); ('ω', "$\\omega$");
]

let fix (s: string) =
    // TODO: unicode -> TeX symbol conversion!
    // (but at least it won't produce syntax errors now)
    List.fold (fun (st: string) (o, n) -> st.Replace(string o, n)) s fixes

