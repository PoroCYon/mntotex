# mntotex

A MuPAD notebook to LaTeX converter. The code makes numerous assumptions,
so this could break if the file format gets updated. (The latter doesn't
seem probable to me, though, as MuPAD is deprecated anyway.)

As always, open an issue if it doesn't work on your file.

## Dependencies

On Linux, OS X, BSD etc., the Mono Runtime has to be installed. (This should
be in the package repos for most distros, as well as in the ports of most
BSDs.)

## Usage

    mntotex [-o <output dir>] <files to convert...>

Or run `mntotex` without args to print a help string.

The `r.sh` script can be used as well, without having to compile.

## Batch conversion

Use the `convmany.sh` shell script:

    ./convmany.sh files...

Or drop the files on the `convmany.sh` file. If the input file is called
`FILE`, the output will be `FILE.tex` in the same directory as the original.

**NOTE: the `mntotex.exe` binary has to be in the same directory
as the `convmany.sh` file!**

## Compilation

    [RELEASE=1] ./c.sh

## License

[SAL](/LICENSE)

